## Domain

CARA is a dialogue system inspired by cognitive-behavioural therapy (CBT), and borrows some techniques of CBT for prompting reflection and the restructuring of negative
thoughts. Essentially, cognitive-affective restructuring is about identifying the links between thoughts and emotions, as well as changing certain thoughts which lead to negative
emotions.
The prototype CARA system interacts with a user in order to identify the following:
- an event
- negative or positive feelings
- thoughts about the event
- restructured thought (if negative)
- gratitude item

## Motivation
Our motivation for creating CARA was to address a key issue in cognitive behaviour
therapy: what happens during the time between sessions? CARA is aimed to assist
those in therapy to continue their restructuring independently, but can also be used by
those who are not in active therapy. Although CBT typically requires participants to
complete written homework, it is less interactive. The goal of CARA is to have a system
which combines both the therapist and the written assignments, without the high cost
of traditional CBT.

## Front-end and Back-end
CARA was prototyped using the dialogue framework OpenDial, a Java-based toolkit. In
the prototype, the backend consisted of a database of positive and negative emotions.
This version of CARA was developed from scratch using Python. This beta version
in Python was manually adjusted and improved after feedback from various test users.
It can fairly accurately take a name input, categorize the emotion input as positive
or negative and respond to user appropriately, as well as take an event, thought, and
gratitude input. For each significant prompt, CARA can accurately identify the response
if there are multiple conditions, and different utterances from the user. The record,
upon the user’s successful completion, is date stamped and stored in a text file. Not
only does CARA help the user to restructure their daily thoughts, but users can also look
back on old records and note their progress. Currently, the beta version only runs in the
command terminal. This project is currently unmaintained. 

### How to run CARA1.0 (command-line interaction only) 
1. download from git: https://gitlab.com/phenixlune/CARA
2. Unzip into folder if necessary
3. Open command terminal
4. cd into folder where all files are saved
5. Run command “python \dialog.py” from containing folder in terminal