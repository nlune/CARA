"""
CARA(beta)
version 1.0
"""
import numpy as np
import string
import datetime
import tkinter

name = ""
feel = "" # the user's unprocessed response
emotions = [] # the feel response processed and broken into a list
pos_emotions = []
neg_emotions = []
events = []
thoughts = []
gratitude = ""
clarify = False
state = "beginning"

def greet(prompt) :
    '''Greets the user and prompts their name input '''
    global name
    unfilled = True
    name = str(input(prompt))
    name = name.strip()
    #keep asking for name if it's an empty input
    while unfilled:
        if len(name) > 0 :
            unfilled = False
        else:
            name = str(input("I didn't get that, please enter your name."))

def parse_name(user_in) :
    '''stores the user name input as name after identifying name portion'''
    global name
    user_input = user_in.strip()
    #check if user started their sentence with my name is, etc
    w = ["t's", " its ", " am ", "I'm", "i'm", " me ", " name ", "name's","Name's", " called ", " is "]
    w1 = ""
    for i in w:
        if i in user_input:
            w1 = i
    #if they did, take name to be words after
    if w1:
        after = user_input[user_input.index(w1) + len(w1):]
        name = after.strip()
    #otherwise use full input as name
    else:
        name = user_input.strip()

    return name

def feeling_prompt(prompt) :
    '''Asks user prompt and takes input to store in feel
        returns feel, the user input without punctuation
    :param prompt: question we want to ask user '''
    global feel
    unfilled = True
    feel = str(input(prompt))
    feel = feel.strip()
    #keep asking for feeling if it's an empty input
    while unfilled:
        if len(feel) > 0:
            #remove punctuation from input
            translator=str.maketrans('','',string.punctuation)
            feel=feel.translate(translator)
            unfilled = False
            return feel
        else:
            print("I didn't get that. Please enter how you're feeling today.")

def read_files(textfile) :
    '''Reads the file for our emotion words to an array
        @param textfile: the name of the file to read_files
    '''
    with open(textfile) as file:
        arr = list(file.read().split())
        #convert all words to lowercase so it's easier to match later
        return list(map(lambda x: x.lower(), arr))


def get_feeling(user_in) :
    '''trims user input to identify emotion variable
        returns list of user input broken down by word
        @param in: user word input, use output from feeling_prompt
    '''
    global emotions
    user_input = user_in.strip()
    w = [" am ", "I'm"]
    w1 = ""
    #check if user started their sentence with I'm feeling, etc
    for i in w:
        if i in user_input:
            w1 = i
    #if they did, take the emotion word to be words after. Add to list
    if w1:
        after = user_input[user_input.index(w1) + len(w1):]
        e = after.strip()

    #otherwise use full input as emotion, add to list
    else:
        e = user_input.strip()

    #if emotion input is more than 1 word, split into a list. add to end of emotions list
    if ' ' in e:
        e = list(e.split())
        e = list(map(lambda x: x.lower(), e))
        #if user input contains not good or not bad, label as "not good" or "not bad"
        if "not" in e:
            if "good" or "great" in e:
                e = list(map(lambda x: x.replace("good", "not good"), e))
                e = list(map(lambda x: x.replace("great", "not great"), e))
            if "bad" in e:
                e = list(map(lambda x: x.replace("bad", "not bad"), e))
            if "ok" in e:
                e = list(map(lambda x: x.replace("ok", "not ok"), e))
            if "well" in e:
                e = list(map(lambda x: x.replace("well", "not well"), e))
        if "don't" and "feel" in e:
            if "good" in e:
                e = list(map(lambda x: x.replace("good", "not good"), e))
            if "bad" in e:
                e = list(map(lambda x: x.replace("bad", "not bad"), e))
            if "ok" in e:
                e = list(map(lambda x: x.replace("ok", "not ok"), e))
            if "well" in e:
                e = list(map(lambda x: x.replace("well", "unwell"), e))


        emotions.extend(e)
    #return list of emotions from most recent input
    else:
        emotions.append(e)
        #make single word input into list form
        i = [e]
        e = i

    return e

def match_feeling(emotion, array):
    '''if emotion from user input matches to our emotion word list, return true.
        Else return false. This function is used to categorize_emotions

        @param emotion: use emotions array elements
        @param array: use read_files to get our list of emotion keywords
    '''
    for i in array:
        if emotion in array:
            return True
        else:
            return False

def categorize_emotions(emo) :
    '''takes elements of list and add to appropriate list depending if positive or negative
        returns a categorization of pos, neg, none, or both
        :param emo: a list of emotions input we have after get_feeling'''
    global pos_emotions, neg_emotions

    '''here are our word list for positive and negative emotions
    You can add any words with spaces below to the files'''
    positive = read_files("positive_emotions.txt")
    positive.append("not bad")
    negative = read_files("negative_emotions.txt")
    negative.append("not good")
    negative.append("not great")
    negative.append("not ok")
    negative.append("not well")
    p = "positive"
    n = "negative"
    b = []
    for i in emo:
        if match_feeling(i, positive):
            pos_emotions.append(i)
            b.append("pos")
        elif match_feeling(i, negative):
            neg_emotions.append(i)
            b.append("neg")
    if "pos" in b and not "neg" in b:
        b = "pos"
    elif "neg" in b and not "pos" in b:
        b = "neg"
    elif "neg" and "pos" in b:
        b = "both"
    else: b = "none"

    return b

def respond_appropriate():
    '''gives user approrpiate response depending on the type of emotion they entered,
            and changes the state so we can move forward accordingly'''
    global clarify
    global state
    if pos_emotions and not neg_emotions:
        state = "event"
        p = np.random.choice(pos_emotions)
        print("I'm glad to hear that you're feeling ", end="", flush=True)
        print(p + ". ", end ="", flush=True)

    if neg_emotions and not pos_emotions:
        state = "neg_event"
        n = np.random.choice(neg_emotions)
        print("I'm sorry to hear that you're feeling ", end="", flush=True)
        print(n + "...", flush=True)

    if pos_emotions and neg_emotions:
        state = "event"
        p = np.random.choice(pos_emotions)
        n = np.random.choice(neg_emotions)
        print("Seems like you're having some mixed feelings. Sorry to hear that you're feeling " + n + ", but at least you're also feeling " + p + "!")

    if not pos_emotions and not neg_emotions:
        state = "clarify"
        feeling_prompt("Sorry, I couldn't understand that, but I do care. Could you repeat how you feel?\n")


def prompt_event(prompt):
    '''prompt event and stores user input in event var'''
    global events
    e = str(input(prompt))
    events.append(e)
    return e
    #print(events) #delete

def prompt_thought(prompt):
    '''prompt user about thoughts regarding event'''
    global thoughts
    t = str(input(prompt))
    thoughts.append(t)
    return t
    #print(thoughts) #del

def boolean_ask(prompt) :
    '''returns "yes", "no", "maybe", or clarify"
    :param prompt: question for user input (ie do you want to continue?)'''
    ans = str(input(prompt))
    ans.lower()
    y = ["yes", "yeah", "sure", "ye", "ok", "I would", "f course", "efinitely"]
    n = [" no ", "nay", "nah", "don't", " not ", " never ", "nothing", "wouldn't"]

    for i in y:
        if i in ans:
            ans = "yes"
    for j in n:
        if j in ans:
            ans = "no"
    return ans

def prompt_gratitude(prompt):
    global gratitude
    gratitude = str(input(prompt))
    return gratitude

def check_nothing(inp):
    #returns True if user says "nothing"
    i = str(inp)
    check = False
    no_words = [" no ","nothing", "don't know","dunno", "didn't do"]
    for word in no_words:
        if word in i:
            check = True
    return check


def write_file():
    '''writes file to save record
    :param name: name of file, can be the date'''
    date = str(datetime.date.today())
    f = open(name + "_" + date + '.txt', 'w')

    f.write("CARA session\n" + date + "\n")
    f.write("\nName: " + name + "\n")
    f.write("\nPositive emotions: \n")
    if pos_emotions:
        for i in pos_emotions:
            f.write(i + " \n")
    else: f.write("nothing here. I hope you'll have more next time :(\n")
    f.write("\nNegative emotions: \n")
    if neg_emotions:
        for i in neg_emotions:
            f.write(i + " \n")
    else: f.write("nothing here - you had a good day :)\n")
    f.write("\nEvents:\n")
    for i in events:
        f.write(i + "\n")
    f.write("\nThoughts:\n")
    for i in thoughts:
        f.write(i + "\n")
    f.write("\nGratitude: \n")
    f.write(gratitude + "\n\n")
    f.write("\nThanks for using CARA, your trusty Cognitive-Affective Behavioural Agenda. See you next time!")

    f.close()

def implement() :
    '''Cognitive Affective Behavioural Agenda/Assitant 1.0 test phase'''
    global state
    #greet user and get their name input
    greet("Hi, I'm CARA, your personal cognitive-behavioural restructuring agenda. Please enter your name.\n")
    parse_name(name)
    print('Nice to meet you '+ name + ' :) ', end="", flush=True)
    feeling_prompt("How are you feeling today?\n")
    get_feeling(feel)

    #print(feel) #testing
    #print(emotions) #testing
    categorize_emotions(emotions)
    respond_appropriate()

    #if emotions were not categorizable, state changed to clarify in respond_appropriate step
    while state=="clarify":
        get_feeling(feel)
        categorize_emotions(emotions)
        respond_appropriate() #keep trying until you get a categorizable emotion, and change to other state

    #this state triggered if we got positive or mixed emotions
    if state=="event":
        b = prompt_event("Tell me about something you did today :)\n")
        none = check_nothing(b)
        while none:
            b = prompt_event("You must've done something today. You can tell me anything.\n")
            none = check_nothing(b)
        feeling_prompt("What are some feelings you had about that?\n")
        state = "categorize"

    while state=="categorize":
        #categorize feeling again and respond appropriately
        c = categorize_emotions(get_feeling(feel)) #output pos, neg, none, or both
        #print(get_feeling(feel)) #test
        #print(c) #test
        if c=="pos":
            ask=True
            #prompt user if they want to input anything else
            b = boolean_ask("I'm happy to hear that. You’re doing very well, but it’s important to continue to write in this agenda. Would you like to tell me anything else?\n" )
            while ask:
                #print(b)#testing
                if b == "yes":
                    state = "continue"
                    break
                if b == "no":
                    state = "gratitude"
                    break
                else:
                    print("I'm not too sure what you'd like to do,", end="", flush=True)
                    b = boolean_ask(" would you like to tell me anything else?\n")


        elif c=="neg":
            state = "neg_thought"

        elif c == "none" :
            feeling_prompt("Sorry I didn't quite get that. Could you please repeat?\n")

        else: #if c=="both"
            print("Sounds like you're having some mixed emotions.",end="", flush=True)
            b = boolean_ask("Did you want to say anything else about that?\n")
            ask=True
            while ask:
                #print(b)#testing
                if b == "yes":
                    state = "continue"
                    break
                if b == "no":
                    state = "gratitude"
                    break
                else:
                    print("I'm not too sure what you'd like to do,", end="", flush=True)
                    b = boolean_ask(" would you like to tell me anything else?\n")

    while state =="continue":
        #if user wants to continue, they can enter an extra thought before next state
        b = prompt_thought("What else would you like to tell me?\n")
        c = categorize_emotions(get_feeling(b))
        if c == "pos":
            print("Fantastic! Thanks for sharing.", end="", flush=True)
            state="gratitude"
        elif c == "neg":
            b =boolean_ask("That doesn't sound good. Would you like to tell me more about this?\n")
            ask=True
            while ask:
                #print(b)#testing
                if b == "yes":
                    state = "continue"
                    break
                if b == "no":
                    state = "gratitude"
                    break
                else:
                    print("I'm not too sure what you'd like to do,", end="", flush=True)
                    b = boolean_ask(" would you like to tell me anything else?\n")
        elif c == "none" :
            b =boolean_ask("Thank you for sharing with me. Would you like to say anything else?\n")
            ask=True
            while ask:
                #print(b)#testing
                if b == "yes":
                    state = "continue"
                    break
                if b == "no":
                    state = "gratitude"
                    break
                else:
                    print("I'm not too sure what you'd like to do,", end="", flush=True)
                    b = boolean_ask(" would you like to tell me anything else?\n")
        else: #if c=="both"
            print("Sounds like you're having some mixed emotions.",end="", flush=True)
            b = boolean_ask("Did you want to say anything else about that?\n")
            ask=True
            while ask:
                #print(b)#testing
                if b == "yes":
                    state = "continue"
                    break
                if b == "no":
                    state = "gratitude"
                    break
                else:
                    print("I'm not too sure what you'd like to do,", end="", flush=True)
                    b = boolean_ask(" would you like to tell me anything else?\n")


    if state=="neg_event":
        prompt_event("What happened today that made you feel " + neg_emotions[0] + "?\n")
        state = "neg_thought"

    if state == "neg_thought":
        print("I can understand how you might've felt " + neg_emotions[0] + "... ", end="", flush=True)
        prompt_thought("What were some thoughts you had regarding what happened?\n")

        prompt_thought("Often our thoughts directly affect how we feel in the moment. How do you think your thoughts affected how you felt?\n")

        prompt_thought("Changing our thoughts can sometimes change outcomes for the better. What's an alternative thought you could've had? \n")

        print("That’s great you can restructure your thoughts about this. It’s important to try to readjust your thoughts in these types of situations.")
        feeling_prompt("What is a good feeling that you had today?\n")
        categorize_emotions(get_feeling(feel))
        b =boolean_ask("Would you like to tell me anything else?\n")
        ask=True
        while ask:
            #print(b)#testing
            if b == "yes":
                state = "continue"
                break
            if b == "no":
                state = "gratitude"
                break
            else:
                print("I'm not too sure what you'd like to do,", end="", flush=True)
                b = boolean_ask(" would you like to tell me anything else?\n")

    if state == "gratitude":
        b = prompt_gratitude("Ok. You did a great job today! Please finish by entering something you're grateful for.\n")
        none = check_nothing(b)
        while none:
            b = prompt_gratitude("You must have something to be grateful for. You can tell me anything.\n")
            none = check_nothing(b)
        state="terminate"

    if state == "terminate":
        print("Fantastic. Thank you for using CARA. I'm happy to be of assistance and I hope to hear from you again soon. Have a wonderful day! :)")



if __name__ == '__main__':
    implement()
    #code below is for debugging
    '''print("Bad feelings:")
    print(neg_emotions)
    print("Good feelings: ")
    print(pos_emotions)'''
    write_file()
